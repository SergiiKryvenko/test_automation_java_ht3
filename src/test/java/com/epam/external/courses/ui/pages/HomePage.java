package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {
    
    private Header header;
    private Footer footer;

    public HomePage(WebDriver driver) {
        super(driver);
        this.header = new Header(driver);
        this.footer = new Footer(driver);
    }
    
    public String getTopLeftPhoneNumberText() {
        return header.getContactPhoneText();
    }
    
    public SignInPage goToLoginPage() {
        logger.info("move to login form");
        return header.clickOnLoginButton();
    }
    
    public SendMessagePopUpPage goToSendMessageForm() {
        logger.info("move to Send Message pop-up form");
        return footer.clickOnSendMessageButton();
    }

}
