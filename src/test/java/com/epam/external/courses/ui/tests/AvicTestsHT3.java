package com.epam.external.courses.ui.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.external.courses.ui.utils.ExplicitWaitUtil;

public class AvicTestsHT3 extends TestRunner {
    
    @DataProvider(name = "Data-Provider-TopLeftPhoneNumber")
    public Object[][] parameterForTopLeftPhoneNumber() {
        return new Object[][] { { "0-800-307-900" } };
    }
    
    @DataProvider(name = "Data-Provider-LoginPageURL")
    public Object[][] parameterForLoginPageURL() {
        return new Object[][] { { "avic.ua/sign-in" } };
    }
    
    @DataProvider(name = "Data-Provider-RegistrationPageURL")
    public Object[][] parameterForRegistrationPageURL() {
        return new Object[][] { { "avic.ua/sign-up" } };
    }
    
    @DataProvider(name = "Data-Provider-SendMessageFormName")
    public Object[][] parameterForSendMessageFormName() {
        return new Object[][] { { "Отправить сообщение" } };
    }

    @Test(priority = 1, dataProvider = "Data-Provider-TopLeftPhoneNumber")
    public void checkTopLeftPhoneNumber(String expected) {
        logger.info("start test checkTopLeftPhoneNumber()");
        String actual = getHomePage().getTopLeftPhoneNumberText();
        logger.info("get phone number: " + actual);
        assertEquals(actual, expected, "the actual phone number didn't match the number 0800307900");
    }

    @Test(priority = 2, dataProvider = "Data-Provider-LoginPageURL")
    public void checkLoginButton(String expectedURL) {
        logger.info("start test checkLoginButton()");
        String actual = getHomePage().goToLoginPage().getUrlAddress();
        logger.info("get URL address: " + actual);
        assertTrue(actual.contains(expectedURL), "the actual URL didn't match the avic.ua/sign-in");
    }

    @Test(priority = 3, dataProvider = "Data-Provider-RegistrationPageURL")
    public void checkRegistrationButton(String expectedURL) {
        logger.info("start test checkRegistrationButton()");
        String actual = getHomePage().goToLoginPage().goToSignUpPage().getUrlAddress();
        logger.info("get URL address: " + actual);
        assertTrue(actual.contains(expectedURL), "the actual URL didn't match the avic.ua/sign-up");
    }

    @Test(priority = 4, dataProvider = "Data-Provider-SendMessageFormName")
    public void checkSendMessageButton(String expected) {
        logger.info("start test checkSendMessageButton()");
        String actual = getHomePage().goToSendMessageForm().getSendMessageFormName();
        logger.info("get message form name: " + actual);
        assertEquals(actual, expected, "the actual message form name didn't match the name 'Отправить сообщение'");
    }
}
