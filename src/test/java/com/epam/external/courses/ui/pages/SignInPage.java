package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    private Header header;
    private Footer footer;

    @FindBy(css = ".container-right a[href*='sign-up']")
    private WebElement signUpButton;

    public SignInPage(WebDriver driver) {
        super(driver);
        this.header = new Header(driver);
        this.footer = new Footer(driver);
    }

    public String getUrlAddress() {
        return driver.getCurrentUrl();
    }

    public SignUpPage goToSignUpPage() {
        logger.info("move to registration form");
        signUpButton.click();
        return new SignUpPage(driver);
    }

}
