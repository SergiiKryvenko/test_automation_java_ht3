package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TopContactsDropdown extends BasePage {

    @FindBy(css = ".contacts-phone>a[href*='0932486711']")
    private WebElement lifePhone;

    @FindBy(css = ".contacts-phone>a[href*='0989079696']")
    private WebElement kyivstarPhone;

    @FindBy(css = ".contacts-phone>a[href*='0952919787']")
    private WebElement mtcPhone;

    //
    public TopContactsDropdown(WebDriver driver) {
        super(driver);
    }

    public String getMtcPhoneText() {
        return mtcPhone.getText();
    }

    public String getKyivstarPhoneText() {
        return kyivstarPhone.getText();
    }

    public String getLifePhoneText() {
        return lifePhone.getText();
    }

}