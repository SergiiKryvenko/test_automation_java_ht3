package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Footer extends BasePage {
    
    @FindBy(css = ".footer-nav a[href*='/blog']")
    private WebElement newsButton;
    
    @FindBy(css = ".footer-nav a[href*='/faq']")
    private WebElement faqButton;
    
    @FindBy(xpath = "//div[@class='footer__nav']//div[contains(@class, 'mobile-hidden')]/button[contains(@class, 'addMessage_btn')]")
    private WebElement sendMessageButton;

    public Footer(WebDriver driver) {
        super(driver);
    }

    public NewsPage clickOnNewsButton() {
        newsButton.click();
        return new NewsPage(driver);
    }
    
    public SendMessagePopUpPage clickOnSendMessageButton() {
        sendMessageButton.click();
        return new SendMessagePopUpPage(driver);
    }
}