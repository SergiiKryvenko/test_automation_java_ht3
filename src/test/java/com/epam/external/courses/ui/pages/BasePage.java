package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.external.courses.ui.utils.ExplicitWaitUtil;

public class BasePage{
    
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected WebDriver driver;
    protected ExplicitWaitUtil wait;
    
    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new ExplicitWaitUtil(driver);
        PageFactory.initElements(driver, this);
    }
}
