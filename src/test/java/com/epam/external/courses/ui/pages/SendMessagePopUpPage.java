package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SendMessagePopUpPage extends BasePage {

    @FindBy(css = "#js_addMessage>.modal-top>.ttl")
    private WebElement formName;

    public SendMessagePopUpPage(WebDriver driver) {
        super(driver);
    }

    public String getSendMessageFormName() {
        wait.visibilityOfWebElement(formName);
        return formName.getText();
    }

}
