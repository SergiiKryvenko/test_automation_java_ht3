package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;

public class NewsPage extends BasePage {
    
    private Header header;
    private Footer footer;

    public NewsPage(WebDriver driver) {
        super(driver);
        this.header = new Header(driver);
        this.footer = new Footer(driver);
    }

}
