package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpPage extends BasePage {

    private Header header;
    private Footer footer;

    @FindBy(css = ".container-right a[href*='sign-in']")
    private WebElement signInButton;

    public SignUpPage(WebDriver driver) {
        super(driver);
        this.header = new Header(driver);
        this.footer = new Footer(driver);
    }

    public String getUrlAddress() {
        return driver.getCurrentUrl();
    }

    public SignInPage goToSignUpPage() {
        logger.info("move to login form");
        signInButton.click();
        return new SignInPage(driver);
    }
}
