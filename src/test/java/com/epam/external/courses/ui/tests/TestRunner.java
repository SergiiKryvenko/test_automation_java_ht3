package com.epam.external.courses.ui.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.epam.external.courses.ui.pages.HomePage;

import io.github.bonigarcia.wdm.WebDriverManager;

public abstract class TestRunner {
	private final Long ONE_SECOND_DELAY = 1000L;
	//
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	protected WebDriver driver;

	@BeforeSuite
	public void beforeSuite() {
		WebDriverManager.chromedriver().setup();
	}

	@BeforeClass
	public void setUpBeforeClass() throws Exception {
	    logger.debug("start setUpBeforeClass()");
        logger.trace("lunch setUpBeforeClass()");
        logger.info("lunch setUpBeforeClass()");
        // Chrome with UI
        driver = new ChromeDriver();
        // Chrome Without UI
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless");
//        driver = new ChromeDriver(options);
        //
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//driver.manage().window().setSize(new Dimension(640, 480));
		//driver.manage().window().setSize(new Dimension(480, 640));
	}

	@AfterClass(alwaysRun = true)
	public void tearDownAfterClass() throws Exception {
//	    presentationSleep(1);
		if (driver != null) {
			driver.quit();
		}
	}

	@BeforeMethod
	public void setUp() throws Exception {
		driver.get("https://avic.ua/");
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		if (!result.isSuccess()) {
		    logger.warn("Test " + result.getName() + " ERROR");
			// Take Screenshot, save sourceCode, save to log, prepare report, Return to previous state, logout, etc.
		}
		// logout, get(urlLogout), delete cookie, delete cache
	}
	
	protected void presentationSleep() {
		presentationSleep(1);
	}
	
	protected void presentationSleep(int seconds) {
		try {
			Thread.sleep(seconds * ONE_SECOND_DELAY); // For Presentation ONLY
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected HomePage getHomePage() {
	    return new HomePage(driver);
	}
}