package com.epam.external.courses.ui.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExplicitWaitUtil {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private WebDriver driver;
    private WebDriverWait wait;

    public ExplicitWaitUtil(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }
    
    public void elementToBeClickable(By locator) {
        logger.trace("wait until an element is clickable");
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        wait.until(ExpectedConditions
                .elementToBeClickable(locator));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
    
    public void visibilityOfWebElement(WebElement webElement) {
        logger.trace("wait until a webElement is visible");
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        wait.until(ExpectedConditions
                .visibilityOf(webElement));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
    
    public void presenceOfElementLocated(By locator) {
        logger.trace("wait until an element is present");
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        wait.until(ExpectedConditions
                .presenceOfElementLocated(locator));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
    
    public void elementToBeSelected(By locator) {
        logger.trace("wait until an element is visible");
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        wait.until(ExpectedConditions
                .elementToBeSelected(locator));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
}
