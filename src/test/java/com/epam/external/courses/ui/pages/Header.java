package com.epam.external.courses.ui.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Header extends BasePage{
    
    private TopContactsDropdown topContactsDropdown;

    @FindBy(xpath = "//input[@id='input_search']")
    private WebElement searchInput;

    @FindBy(xpath = "//span[@class='sidebar-item']")
    private WebElement productCatalogButton;

    @FindBy(xpath = "//div[contains(@class,'header-bottom__cart')]//div[contains(@class,'cart_count')]")
    private WebElement amountOfProductsInCart;

    @FindBy(css = ".header-top__item a[href*='0800307900']")
    private WebElement phoneNumber;
    
    @FindBy(css = ".js-cont-btn")
    private WebElement phoneContacts;

    @FindBy(css = ".header-bottom__logo>img")
    private WebElement logo;
    
    @FindBy(css = ".balance-btn+a[href*='sign-in']")
    private WebElement loginButton;

    public Header(WebDriver driver) {
        super(driver);
        this.topContactsDropdown = new TopContactsDropdown(driver);
    }

    public SearchResultsPage searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
        return new SearchResultsPage(driver);
    }

    public void clickOnProductCatalogButton() {
        productCatalogButton.click();
    }

    public String getTextOfAmountProductsInCart() {
        return amountOfProductsInCart.getText();
    }
    
    public String getContactPhoneText() {
        return phoneNumber.getText();
    }
    
    public HomePage clickOnLogo() {
        logo.click();
        return new HomePage(driver);
    }
    
    public String getContactLifePhoneNumber() {
        Actions action = new Actions(driver);
        action.moveToElement(phoneContacts).build().perform();
        return topContactsDropdown.getLifePhoneText();
    }
    
    public SignInPage clickOnLoginButton() {
        loginButton.click();
        return new SignInPage(driver);
    }
}